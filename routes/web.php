<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','modulos\HomeController@index')->name('home');

Route::get('/movil', 'modulos\MovilController@index')->name('movil');

// rutas callback
Route::post('/callback-v1','CallbackController@Callback_v1');
Route::post('/callback-v2','CallbackController@Callback_v2');