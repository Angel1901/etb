<?php
return [
	"url"=> 'https://so02.kerberusipbx.com:625/api/v0.1/callbackv2',
	"fijo"=> [
				'cola' => 'MX2',
			    'troncal' => 'market',
			    'intentos' => 2,
			    'tiempoEspera' => 20,
			    'amd' => false,
			    'autorizacion' => 'c21f969b5f03d33d43e04f8f136e7682'
			 ],
	"movil"=> [
				'cola' => 'MX2',
			    'troncal' => 'market',
			    'intentos' => 2,
			    'tiempoEspera' => 20,
			    'amd' => false,
			    'autorizacion' => 'c21f969b5f03d33d43e04f8f136e7682'
			 ]
];
