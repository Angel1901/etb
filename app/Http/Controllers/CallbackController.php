<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CallbackController extends Controller
{
    public function Callback_v1(Request $request){
    	$config= config('callback_v1');
    	$tipo= !empty($request->request->get('tipo')) ? $request->request->get('tipo') : 'movil';
    	$numero= !empty($request->request->get('numero')) ? $request->request->get('numero') : '3168285147';
    	$extra= !empty($request->request->get('extra')) ? $request->request->get('extra') : array();

    	if($numero != ''){
    		$url= $config['url'];
	    	$configEnvio= $config[$tipo];
	    	$datosEnvio= [
	    					"numero"=> $numero,
	    					"id"=> $configEnvio['id'],
	    					"extra"=> $extra
	    				 ];
	    	return json_encode($this->enviarDatosV1($url, $datosEnvio));
    	}else{
    		return json_encode('ERROR DATOS');
    	}
    }
 
    public function Callback_v2(Request $request){
		$config= config('callback_v2');
    	$tipo= !empty($request->request->get('tipo')) ? $request->request->get('tipo') : 'movil';
    	$numero= !empty($request->request->get('numero')) ? $request->request->get('numero') : '3168285147';
    	$extra= !empty($request->request->get('extra')) ? $request->request->get('extra') : array();

    	if($numero != ''){
    		$url= $config['url'];
	    	$configEnvio= $config[$tipo];
	    	$autorizacion= $configEnvio['autorizacion'];
	    	$datosEnvio= [
	    					"numero"=> $numero,
						    'cola' => $configEnvio['cola'],
						    'troncal' => $configEnvio['troncal'],
						    'intentos' => $configEnvio['intentos'],
						    'tiempoEspera' => $configEnvio['tiempoEspera'],
						    'amd' => $configEnvio['amd'],
						    'extra' => $extra
	    				 ];
	    	return json_encode($this->enviarDatosV2($url, $datosEnvio, $autorizacion));
    	}else{
    		return json_encode('ERROR DATOS');
    	}   	
    }

    public function enviarDatosV1($url, $datosEnvio=[]){
    	// use key 'http' even if you send the request to https://...
		$opts = array('http' =>
		    array(
		        'method' => 'POST',
		        'header' => "Content-type: application/json\r\n",
		        'content' => json_encode($datosEnvio),
		    )
		);
		$context  = stream_context_create($opts);
		$result = file_get_contents($url, false, $context);
		
		return $result === FALSE ? 'ERROR' : $result;		
    }

    public function enviarDatosV2($url, $datosEnvio=[], $autorizacion=''){
    	// use key 'http' even if you send the request to https://...
		$opts = array('http' =>
		    array(
		        'method' => 'POST',
		        'header' => "Authorization: ".$autorizacion."\r\n" . 
		                    "Content-type: application/json\r\n" .
		                    "Connection: close\r\n" .
		                    "Content-length: " . strlen(json_encode($datosEnvio)) . "\r\n",
		        'content' => json_encode($datosEnvio)
		    )
		);
		$context  = stream_context_create($opts);
		$result = file_get_contents($url, false, $context);
		
		return $result === FALSE ? 'ERROR' : $result;
    }
}
