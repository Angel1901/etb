<?php

namespace App\Http\Controllers\modulos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $modulo='home';

    public function index(){  
		
		return view('landing/'.$this->modulo.'/index',[ 
								'css' => ['landing/'.$this->modulo.'/estilos.css'],
								'js' => ['landing/'.$this->modulo.'/script.js'],
								'ciudades' => require('utilidad/ciudades.php'),
								'horario_atencion' => require('utilidad/horario_atencion.php'),
								'lista_negra' => require('utilidad/lista_negra.php'),
								'tsourse' => require('utilidad/tsourse.php'),
								'modulo' => $this->modulo
						   ]);
	}
}
