<footer>
  <div class="container">
    <div class="row pt-5 m-0 p-0">
        <div class="col-sm-12 col-md-12 col-lg-6">
          <h2 class="DINNextLTPro-Black dejanos">Déjanos tus datos</h2>
          <p class="DINNextLTPro-Light fz20 textGray2">O también puedes llamarnos en Bogotá al:</p>
          <p class="DINNextLTPro-Bold numeros "><a class="textOrange" href="tel:0314434040">4434040</a></p>
          <p class="DINNextLTPro-Bold numeros m-0"><a class="textOrange" href="tel:3009121295">3009121295</a></p>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 formp0">
          <form class="form-callback marginForm" id="form-footer">
            <div class="col-md-12 col-sm-12 col-12 row m-0 formp0"> 
              <div class="col-md-12 col-sm-12 col-12 formp0">
                <div class="m-0 p-0">
                  <div class="form-group col-md-12 col-sm-12 cronos margen w-100 formp0">
                    <label class="DINNextLTPro-Medium textOrange" for="">NOMBRE *</label>
                    <input type="text" class="form-control border" name="nombre" id="" placeholder="" required="">
                  </div>
                  <div class="form-group col-md-12 col-sm-12 cronos margen w-100 formp0">
                    <label class="DINNextLTPro-Medium textOrange" for="">TELÉFONO*</label>
                    <input type="text" class="form-control border" name="numero" id="inputPassword4" placeholder="" required="">
                  </div>
                  <div class="form-group col-md-12 col-sm-12 cronos margen w-100 formp0">
                    <label class="DINNextLTPro-Medium textOrange" for="">E-Mail*</label>
                    <input type="email" class="form-control border" name="email" id="inputCity" placeholder="" required="">
                  </div>
                  <div class="form-group col-md-12 col-sm-12 cronos margen w-100">
                    <label class="DINNextLTPro-Medium " for="">
                      <input class="form-check-input" type="checkbox" id="check_2" required="">
                      <a href="#" class="textOrange">Acepto uso de Datos</a>
                    </label>
                  </div>
                  <div class="form-group col-md-12 col-sm-12 cronos margen w-100">
                    <label class="DINNextLTPro-Medium textOrange" for="">
                      <input class="form-check-input" type="checkbox" id="check_3" required="">
                      <a href="#" class="textOrange">Acepto el uso de datos personales</a></label>
                  </div>
                  <div class="form-group col-md-12 col-sm-12 cronos margen w-100 formp0">
                    <label class="DINNextLTPro-Medium textOrange terminosfooter" for="">Autorizo a ETB para usar mis datos personales con la I) Comunicar eficientemente información propia de ETB, así como de nuestras filiales y/o aliados comerciales, sobre productos, servicios, ofertas. II) Informar sobre nuevos productos o servicios que estén relacionados con el o los servicios adquiridos. III) Evaluar la calidad del o los servicios. IV) Informar sobre cambios de nuestros productos o servicios. V) Participar en programas de lealtad con beneficios. VI)Realizar estudios de mercado sobre hábitos de consumo. VII) Transferir y transmitir datos personales a terceros con vínculos comerciales con ETB. VIII) Las demás finalidades estrechamente asociadas y necesarias para cumplir los fines de mercadeo.</label>
                  </div>
                  <div class="form-group col-md-12 col-sm-12 cronos margen w-100 formp0">
                    <input class="w-75 btn btn-primary DINNextLTPro-Medium textWhite backgroundOrange border p-3" type="submit" value="ENVIAR">
                  </div>  
                </div>
              </div>
            </div> 
          </form>
        </div>
        <div>
          <label><img class="logoETB2" src="../img/logoetb.png"></label>
        </div>
    </div>
  </div>
  <div class="w-100 backgroundOrange">
    <label class="textWhite DINNextLTPro-Medium text-center w-100">SOL SAS Distribuidor autorizado de ETB </label>
  </div>
</footer>
  
