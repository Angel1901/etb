@extends('general.general')

@section('nav')
    @include('general.nav')
@endsection

@section('header')
    @include('landing.'.$modulo.'.header')
@endsection

@section('content')
    @include('landing.'.$modulo.'.contenido')
    @include('general.modals')
@endsection

@section('footer')
    @include('landing.'.$modulo.'.footer')
    @include('general.gracias')
    @include('general.fuera_horario')
@endsection
