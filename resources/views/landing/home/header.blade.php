<!-- Header -->

<header class="banner d-block d-sm-block d-md-block d-lg-block">
	<div class="col-12 d-block d-sm-block d-md-none d-lg-none css-nav-movil-height"></div>
	<img class="d-none d-sm-none d-md-block d-lg-block banner__imagen" src="../img/FIBRA-HOGAR-2-min.png">
	<img class="d-block d-sm-block d-md-none d-lg-none w-100" src="../img/Bannernuevomobilehogares-2.png">
<div class="banner_Hogar banner__content pt-5">
	<div class="row	m-0">
		<div class="w-50 img-Encont d-none d-sm-none d-md-block d-lg-block">
			<img class="h-100" src="../img/Objeto-inteligente-vectorial-1.png">
		</div>
		<div class="w-50 pt-5 row m-0 ">
				<div class="d-none d-sm-none d-md-block d-lg-block col-9 p-0">
					@include('general.form-principal')			
				</div>
				<div class="col-sm-12 col-md-12 col-lg-3 p-0">
					<span class=""><img class="d-none d-sm-none d-md-block d-lg-block  logoETB2" src="../img/ETB.png"></span>
					<a href=""><img class="negocios" src="../img/Group-17.png"></a>
					<a href=""><img class="movil" src="../img/Group-311.png"></a>
				</div>
		</div>
	</div>
</div>	
<div class="d-block d-sm-block d-md-none d-lg-none backgroundBlue3 w-100 pt-3 pb-3">
	<div class="pt-5 pb-5">
		@include('general.principalmovil')
	</div>
</div>
</header>
