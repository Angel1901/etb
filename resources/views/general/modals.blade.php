<div class="container">
		<div class="modal fade registration-form-modal-general form-request-modal" id="fjklesdjsl">        
			<div class="modal-dialog modal-md">
				<div class="modal-content" style="max-width: 26rem !important;">
					<div class="modal-body col-12 text-center p-0 m-0">
						<div class="m-0">
							<div class="form-fixed-principal col-lg-12 p-0 m-0 w-100 card formBanner text-center" id="form-principal">											
								<form id="formulario-modal1" class="col-12 row m-0 p-0 w-100 form-callback">	
									<div class="formHeader cBlanco p-3 w-100">
										<h6>¿Quieres que te llamemos?</h6>
										<h6>Ingresa tu teléfono y te llamaremos de inmediato</h6>	
										<div class="col-12 row m-0 text-center mb-2">
											<label class="ml-auto">fijo</label>
											<label class="ml-2">
											<input type="radio" name="tipo_llamada" value="fijo" onclick="ocultarMovil(false,'formulario-modal1')" class="" required="required">
											</label>
								
											<label class="ml-3">movil</label>
											<label class="ml-2 mr-auto">
											<input type="radio" name="tipo_llamada" value="movil" onclick="ocultarMovil(true,'formulario-modal1')" class=""  checked="checked">
											</label>
										</div>
									</div>

									<div class="formBody w-100 p-3">
										<div class="col-12 row m-0 mb-2 ocultarMovil d-none">
											<div class="col-12">
												<select class="form-control" name="ciudad">
												@for($i=0; $i<count($ciudades);$i++)
												<option value="{{$ciudades[$i]['indicativo']}}">{{$ciudades[$i]['nombre']}}</option>	          
												@endfor	          
												</select>
											</div>
										</div>
								
										<div class="col-12 row m-0 mb-2">
											<div class="col-12">
											<input type="text" name="numero" class="form-control" placeholder="numero" required="required">
											</div>
										</div>

										<div class="form-check cronos">
											<input class="form-check-input"  type="checkbox" id="check_1" required="">
											<label class="form-check-label cAzul cronos" id="check_label_1" for="check_1">
											Acepto <a href="documentos/TyC_Oferta_EspecialO.pdf" class="" target="_blank"> Términos y condiciones</a> 
											</label>
										</div>
									</div>
									<div class="formHeader w-100 p-3">
										<button type="submit" class="btn btn-primary formBody cAzul cronosBold">ESTOY INTERESADO</button>
									</div>
								</form>  
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>