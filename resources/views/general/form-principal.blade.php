<div class="col-lg-3 col-sm-12 col-12 boxTeLlamamos">
	<div class="">   
		<div class="">
			<p class="boxText">¡Contrata Ahora!</p>
		</div>
		<div class="boxblank align-items-center p-4">
			<label class="boxTeLlamamos__info ">Ingresa tu teléfono de contacto móvil o fijo</label>
			<form id="formulario-principal" class="col-12 p-0">
				<div class="col-12 row m-0 mb-2 p-0">
					<div class="col-12 p-0">
					<input type="number" name="numero" class="form-control text-center inputForm" required="required">
					</div>
				</div>
				<div class="form-check check cronos m-2">
					<input class="form-check-input" type="checkbox" id="check_1" required="">
					<span class="form-check-label cAzul cronos" id="check_label_1" for="check_1">
					<a href="documentos/politicaProteccionDatosPersonales.pdf" class="textGray boxTeLlamamos__terms	terminos" target="_blank"> Acepto el uso de datos personales</a></span>
					<label><a href="https://etb.com/tyc.aspx" class="textGray terminos" target="_blank"> Aplica términos y condiciones</a> 
					</label>
				</div>
				<button class="btn btn-primary botonForm btn-form boxText p-0 h-25">Te Llamamos</button>
			</form>                            
		</div>  
	</div>
</div>