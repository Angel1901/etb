<div class="d-none d-sm-none d-md-block d-lg-block">
<div class="blue-header-hogar w-100">
  <ul class="nav logoHeader_hogar">
      <li class="nav-item d-none d-lg-block mr-5">
          <a class="nav-link" href="/"><img class="" src="/img/LogoHeader.png" alt=""></a>     
      </li>
  </ul>
</div>
<div class="boxllamada">
  		<div class="p10 ">
  		  <img class="tel" src="/img/ETBHEADER.png" alt="">
  		</div>
  		<div class="boxTelefonos size23 row m-0 col-5 p-0 text-center">
  			<label class="m-0 textWhite ">LÍNEA DE VENTAS</label>	
  			<label class=""><a class=" textWhite boxTelefonos" href="tel:3009121295">3009121295</a></label>
  			<label class=""><a class=" textWhite boxTelefonos telFijo" href="tel:0314434040">(031)443 4040</a></label>
  		</div>
</div>
</div>
<!--Movil-->
<div class="col-12 d-block d-sm-block d-md-none d-lg-none backgroundBlue css-nav-movil">
    <div class="row m-0 p-0">
      <div class="col-3 w-100 p-0 pb-4 pt-4">
        <a class="" href="/">
          <img class="w-50" src="../img/LogoHeader.png">
        </a>     
      </div>
      <div class="col-5 w-100 p-0 row align-items-center">
        <label class="textWhite">LÍNEA DE VENTAS</label>
      </div>
      <div class="col-4 w-100 p-0 row justify-content-end">
        <label class="">
          <a class=" textWhite boxTelefonos" href="tel:3009121295">3009121295</a>
          <a class=" textWhite boxTelefonos telFijo" href="tel:0314434040">(031)443 4040</a>
        </label>
      </div>
    </div>
</div>