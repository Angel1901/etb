$(document).ready(function(){
  envioFormulariosCallback();
});

 function getGETUrl(){
        // capturamos la url
        var loc = document.location.href;
        // si existe el interrogante
        if(loc.indexOf('?')>0)
        {
            // cogemos la parte de la url que hay despues del interrogante
            var getString = loc.split('?')[1];
            // obtenemos un array con cada clave=valor
            var GET = getString.split('&');
            var get = {};
 
            // recorremos todo el array de valores
            for(var i = 0, l = GET.length; i < l; i++){
                var tmp = GET[i].split('=');
                get[tmp[0]] = unescape(decodeURI(tmp[1]));
            }
            return get;
        }else{
          return {};
        }
    }


function envioFormulariosCallback(){
  // para el envio Callback1 parametros (cola,url)
  const callback1= new Callback('/callback-v2');
  callback1.setColaDefault("movil");
  // para el envio Callback2 parametros (nombre_cola,url,intentos,tiempoEspera,amd,troncal)
  // const callback2= new Callback2('/callback-v2');
  // callback2.setColaDefault("movil");
  // funcciones de utilidad como llo son lista negra, horario de atencion, tsorce ..... 
  const utilidad= new Utilidad();
  utilidad.cargarTsource('movil'); // funcion para cargar tsource

  // para envio formulario callback principal
  $("#formulario-principal").submit(function(event){
    // if (utilidad.validarHorarioAtencion()) { 
      // se envia el elemeto form y evento
      callback1.setVariablesPorFormulario($("#formulario-principal"),event);
      callback1.setExtra({tsource:window.location.href}); 
      // if(utilidad.validarListaNegra(callback1.getNumero())){ 
        callback1.enviar();
      // }  
      utilidad.gracias();
    // }else{
    //   utilidad.fueraHorario();
    // }
  });

  // para envio formulario callback Modal
  $("#form-contato").submit(function(event){
    // if (utilidad.validarHorarioAtencion()) {
      // se envia el elemeto form y evento
      callback1.setVariablesPorFormulario($("#form-contato"),event);
      callback1.setExtra({tsource:window.location.href});
      // if(utilidad.validarListaNegra(callback1.getNumero())){ 
        callback1.enviar();
      // }  
      utilidad.gracias();
    // }else{
    //   utilidad.fueraHorario();
    // }
  });

  // para envio formulario callback Modal
  $("#formulario-modal1").submit(function(event){
    // if (utilidad.validarHorarioAtencion()) {
      // se envia el elemeto form y evento
      callback1.setVariablesPorFormulario($("#formulario-modal1"),event);
      callback1.setExtra({tsource:window.location.href});
      // if(utilidad.validarListaNegra(callback1.getNumero())){ 
        callback1.enviar();
      // }  
      utilidad.gracias();
    // }else{
    //   utilidad.fueraHorario();
    // }
  });

  // para envio formulario callback Modal
  $("#form-footer").submit(function(event){
    // if (utilidad.validarHorarioAtencion()) {
      // se envia el elemeto form y evento
      callback1.setVariablesPorFormulario($("#form-footer"),event);
      callback1.setExtra({tsource:window.location.href});
      // if(utilidad.validarListaNegra(callback1.getNumero())){ 
        callback1.enviar();
      // }  
      utilidad.gracias();
    // }else{
    //   utilidad.fueraHorario();
    // }
  });
}

function ocultarMovil(tipo=true,idElemento){
    if(!tipo){
      $("#"+idElemento+" .ocultarMovil").removeClass("d-none");
    }else{
      $("#"+idElemento+" .ocultarMovil").addClass("d-none");
    }
}

